import moment from 'moment'

export default {
  filters: {
    datetime: function (utcTime) {
      var offset = moment().utcOffset();
      var localText = moment.utc(utcTime).utcOffset(offset).format('DD/MM/YYYY HH:mm');
      return localText;
    },
  },
  create: function (Vue) {
    Object.keys(this.filters).forEach(function (filter) {
      Vue.filter(filter, this.filters[filter])
    }.bind(this))
  }
}
