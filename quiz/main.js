// Assuming you have already done "npm install fernet"
let fernet = require('fernet')
let secret = new fernet.Secret('TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM=')
// Oh no! The code is going over the edge! What are you going to do?
let message = 'gAAAAABcUjwMyBqY9kgLv3yCqCkIfSeWeBBtJsvRIyqYY-EsNILKjr5FS-6gQZWcqagSSA9zNldg-fCZBAnsnKAM-CvaCw4WnCTfrDO47uOasByNqU6iEidjqcTwhZnW6PlDJkicxzijwPYMqeJ6BukaIAzAK6e-Ms0WQalH1B-3e8yHJjVn1iltmykjFksScFl0VgOj6O_l'
let token = new fernet.Token({secret: secret, token: message, ttl:0})
console.log(token.decode())